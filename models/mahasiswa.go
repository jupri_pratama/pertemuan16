package models

import (
	"time"
)

type (
	Mahasiswa struct {
		ID       int       `json:"id"`
		NIM      int       `json:"nim"`
		Name     string    `name:"name"`
		Semester int       `json:"semester"`
		CreateAt time.Time `json:"created_at"`
		UpdateAt time.Time `json:"updated_at"`
	}

	// type Users struct {
	// 	Id        string `form:"id" json:"id"`
	// 	FirstName string `form:"firstname" json:"firstname"`
	// 	LastName  string `form:"lastname" json:"lastname"`
	// }

	Response struct {
		Status    int         `json:"status"`
		Message   string      `json:"message"`
		Mahasiswa []Mahasiswa `json:"items"`
	}
)
