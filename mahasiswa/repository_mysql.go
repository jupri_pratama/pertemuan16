package mahasiswa

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"pertemuan16/config"
	"pertemuan16/models"
)

const (
	table          = "mahasiswa"
	layoutDataTime = "2006-01-02 15:04:05"
)

// GET ALL
func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {

	var mahasiswas []models.Mahasiswa
	//ini
	// var mahasiswas []models.Response

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant Connect To MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v ORDER BY id DESC", table)

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa models.Mahasiswa
		var createAt, updateAt string

		if err = rowQuery.Scan(&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createAt,
			&updateAt); err != nil {
			return nil, err
		}

		mahasiswa.CreateAt, err = time.Parse(layoutDataTime, createAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswa.UpdateAt, err = time.Parse(layoutDataTime, updateAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)

	}
	return mahasiswas, nil

}

func Insert(ctx context.Context, mhs models.Mahasiswa) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (nim,name,semester,created_at,updated_at) VALUES(%v,'%v','%v','%v','%v')", table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		time.Now().Format(layoutDataTime),
		time.Now().Format(layoutDataTime))

	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, mhs models.Mahasiswa) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nim = %d, name ='%s', semester = %d, updated_at = '%v' WHERE id = '%d'",
		table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		time.Now().Format(layoutDataTime),
		mhs.ID,
	)
	fmt.Println(queryText)

	// _, err = db.ExecContext(ctx, queryText)
	b, err := db.ExecContext(ctx, queryText)

	// if err != nil {
	// 	return err
	// }

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := b.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}

func Delete(ctx context.Context, mhs models.Mahasiswa) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, mhs.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}
